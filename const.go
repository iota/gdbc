package gdbc

import "gitea.com/iota/iota/core"

const (
	// ErrFilterParamEmpty Query参数为空
	ErrFilterParamEmpty = 20001
	// ErrUpdateObjectEmpty 更新参数为空
	ErrUpdateObjectEmpty = 20002
	// ErrUpdateObjectNotSupport 更新参数类型不支持
	ErrUpdateObjectNotSupport = 20003
	// ErrOpTransaction 事务执行错误
	ErrOpTransaction = 20004
)

var (
	coreCodeMap = map[int32]string{
		ErrFilterParamEmpty:       "query param is empty",
		ErrUpdateObjectEmpty:      "update object is empty",
		ErrUpdateObjectNotSupport: "update object type not support",
		ErrOpTransaction:          "abort transaction failed",
	}
)

var (
	ErrRecordNotFound = core.ErrMsg{
		ErrCode: core.ErrRecordNotFound,
		ErrMsg:  core.GetErrMsg(core.ErrRecordNotFound),
	}

	ErrRequestBroken = core.ErrMsg{
		ErrCode: core.ErrRequestBroken,
		ErrMsg:  core.GetErrMsg(core.ErrRequestBroken),
	}

	ErrFilterEmpty = core.ErrMsg{
		ErrCode: ErrFilterParamEmpty,
		ErrMsg:  core.GetErrMsg(ErrFilterParamEmpty),
	}

	ErrObjectEmpty = core.ErrMsg{
		ErrCode: ErrUpdateObjectEmpty,
		ErrMsg:  core.GetErrMsg(ErrUpdateObjectEmpty),
	}

	ErrObjectTypeNotSupport = core.ErrMsg{
		ErrCode: ErrUpdateObjectNotSupport,
		ErrMsg:  core.GetErrMsg(ErrUpdateObjectNotSupport),
	}

	ErrRollbackTransaction = core.ErrMsg{
		ErrCode: ErrOpTransaction,
		ErrMsg:  core.GetErrMsg(ErrOpTransaction),
	}
)

func register() {
	core.RegisterError(coreCodeMap)
}
