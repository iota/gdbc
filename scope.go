package gdbc

import "gorm.io/gorm"

type Scope struct {
	*Model // 操作对象
}

func (s *Scope) RawDB() *gorm.DB {
	if std == nil {
		panic("please call InitClient() to initial mysql client.")
	}
	return std
}

// Insert 执行创建动作
func (s *Scope) Insert() *InsertScope {
	return &InsertScope{scope: s}
}

// Delete 执行删除动作
func (s *Scope) Delete() *DeleteScope {
	return &DeleteScope{}
}

// Update 执行更新动作
func (s *Scope) Update() *UpdateScope {
	return &UpdateScope{}
}
