package gdbc

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/sirupsen/logrus"
	"reflect"
)

type InsertScope struct {
	scope        *Scope
	ctx          context.Context
	err          error
	omitFields   []string
	selectFields []string
	chunkSize    int64
	withPK       bool        // 开启绑定主键id
	pkID         interface{} // 主键id
	rowsAffected *int64      // 插入影响行数
}

// SetContext 设置上下文
func (s *InsertScope) SetContext(ctx context.Context) *InsertScope {
	s.ctx = ctx
	return s
}

// SetOmit 设置插入时忽略传入的指定字段
func (s *InsertScope) SetOmit(omits []string) *InsertScope {
	s.omitFields = omits
	return s
}

// SetSelect 设置插入时只插入传入的指定字段
func (s *InsertScope) SetSelect(selects []string) *InsertScope {
	s.selectFields = selects
	return s
}

// SetChunk 在批量插入时 设置批次大小
func (s *InsertScope) SetChunk(size int64) *InsertScope {
	s.chunkSize = size
	return s
}

// WithPrimaryKey 插入时传入主键变量 该方法会获取到结果后 将主键写入变量中
// 支持类型: *int64,*string,*[]int64,*[]string
// 如果主键类型和设置的类型不一致 会报错[即使插入成功]
func (s *InsertScope) WithPrimaryKey(pk interface{}) *InsertScope {
	vt := reflect.TypeOf(pk)
	if vt.Kind() != reflect.Ptr {
		s.err = fmt.Errorf("bind primary key type must ptr")
		return s
	}
	vt = vt.Elem()

	field := findStructPrimaryKeyField(reflect.New(s.scope.modelKind.Elem()).Interface())
	if !field.pkExist {
		s.err = fmt.Errorf("get primary key type failed")
		return s
	}

	if vt.Kind() == reflect.Slice || vt.Kind() == reflect.Array {
		vt = vt.Elem()
	}

	switch vt.Kind() {
	case reflect.Int64, reflect.String:
		if field.typeOf.Kind() != vt.Kind() {
			s.err = fmt.Errorf("table pk type: %+v, but bind pk type: %+v", field.typeOf.Kind(), vt.Kind())
			return s
		}
	default:
		logrus.Infof("field: %+v, vt: %+v", field.typeOf.Kind(), vt.Kind())
		s.err = fmt.Errorf("unsupport primary key type")
		return s
	}

	s.withPK = true
	s.pkID = pk
	return s
}

// WithRowsAffected 绑定插入时的影响行数
func (s *InsertScope) WithRowsAffected(ra *int64) *InsertScope {
	s.rowsAffected = ra
	return s
}

func (s *InsertScope) bindOnePrimaryKey(pid interface{}) *InsertScope {
	pidType := reflect.TypeOf(pid)
	pidValue := reflect.ValueOf(pid)
	if pidType.Kind() == reflect.Ptr {
		pidType = pidType.Elem()
		pidValue = pidValue.Elem()
	}
	pkType := reflect.TypeOf(s.pkID)
	if pkType.Kind() == reflect.Ptr {
		pkType = pkType.Elem()
	}

	if pkType.Kind() != pidType.Kind() {
		s.err = fmt.Errorf("table %s primary key type is: %+v, bind pk type: %+v", s.scope.tableName, pidType.Kind(), pkType.Kind())
		return s
	}

	pkvo := reflect.ValueOf(s.pkID)
	if pkvo.Kind() == reflect.Ptr {
		pkvo = pkvo.Elem()
	}
	if !pkvo.CanSet() {
		s.err = fmt.Errorf("bind pk type: %+v, cant set", reflect.TypeOf(s.pkID).Name())
		return s
	}

	pkvo.Set(pidValue)
	return s
}

func (s *InsertScope) bindManyPrimaryKey(obj interface{}) *InsertScope {
	// 先检测一下 绑定的是否是 []string []int64
	pkType := reflect.TypeOf(s.pkID)
	if pkType.Kind() != reflect.Ptr {
		s.err = fmt.Errorf("bind primary key type must ptr")
		return s
	}
	pkType = pkType.Elem()
	if pkType.Kind() != reflect.Slice && pkType.Kind() != reflect.Array {
		s.err = fmt.Errorf("bind primary key err: primary key type must be slice")
		return s
	}

	field := findStructPrimaryKeyField(newModelValue(s.scope.modelKind.Elem()).Interface())

	vo := reflect.ValueOf(obj)
	if vo.Kind() == reflect.Ptr {
		vo = vo.Elem()
	}

	if vo.Kind() != reflect.Slice && vo.Kind() != reflect.Array {
		s.err = fmt.Errorf("bind primary key err: insert fillback value not slice")
		return s
	}

	var realPK = reflect.Indirect(reflect.ValueOf(s.pkID))
	var pkVal = reflect.Indirect(reflect.ValueOf(s.pkID))
	for i := 0; i < vo.Len(); i++ {
		node := vo.Index(i)
		if node.Kind() == reflect.Ptr {
			node = node.Elem()
		}
		switch node.Kind() {
		case reflect.Struct:
			pkT := node.FieldByName(field.fieldName)
			if pkT.IsValid() && !pkT.IsZero() {
				pkVal = reflect.Append(pkVal, pkT)
			}
		case reflect.Map:
			pkT := node.MapIndex(reflect.ValueOf(field.columnName))
			if pkT.IsValid() && !pkT.IsZero() {
				pkVal = reflect.Append(pkVal, pkT)
			}
		}
	}

	if realPK.CanSet() {
		realPK.Set(pkVal)
	}

	return s
}

// insertOneMap 插入一个map
// 单插map获取id思路: map转入 struct 中 插入struct后 将 struct 中获取的 id回写入 map 中返回
func (s *InsertScope) insertOneMap(obj interface{}) {
	t, err := json.Marshal(obj)
	if err != nil {
		s.err = err
		return
	}
	recordType := s.scope.modelKind
	if recordType.Kind() == reflect.Ptr {
		recordType = recordType.Elem()
	}

	var record = reflect.New(recordType).Interface()
	if err := json.Unmarshal(t, record); err != nil {
		s.err = err
		return
	}
	s.insertOneStruct(record)
	if s.err != nil {
		return
	}
	// 找 record 的主键在哪里
	field := findStructPrimaryKeyField(record)
	if !field.pkExist {
		return
	}

	// 找到主键后 写入map 返回
	vo := reflect.ValueOf(obj)
	if vo.Kind() == reflect.Ptr {
		vo = vo.Elem()
	}
	if vo.CanSet() {
		if field.columnName != "" {
			vo.SetMapIndex(reflect.ValueOf(field.columnName), field.valueOf)
		}
	}

	// 写回 WithPrimaryKey
	if s.withPK {
		s.bindOnePrimaryKey(field.interfaceValue)
	}
}

// insertManyMap 插入多个map
// 单插map获取id思路: map转入 struct 中 插入struct后 将 struct 中获取的 id回写入 map 中返回
func (s *InsertScope) insertManyMap(obj interface{}) {
	t, err := json.Marshal(obj)
	if err != nil {
		s.err = err
		return
	}
	recordType := s.scope.modelKind
	if recordType.Kind() == reflect.Ptr {
		recordType = recordType.Elem()
	}

	var records = reflect.New(reflect.SliceOf(s.scope.modelKind)).Interface()

	if err := json.Unmarshal(t, records); err != nil {
		s.err = err
		return
	}
	s.insertManyStruct(records)
	if s.err != nil {
		return
	}
}

// insertOneStruct 插入一个struct
func (s *InsertScope) insertOneStruct(obj interface{}) {
	var tx = std.WithContext(s.ctx).Model(reflect.New(s.scope.modelKind).Interface())
	if len(s.omitFields) != 0 {
		tx.Omit(s.omitFields...)
	}
	if len(s.selectFields) != 0 {
		tx.Select(s.selectFields)
	}
	s.err = tx.Create(obj).Error
	if s.err != nil {
		return
	}

	if s.rowsAffected == nil {
		s.rowsAffected = new(int64)
	}
	*s.rowsAffected = tx.RowsAffected

	// 写回 WithPrimaryKey
	if s.withPK {
		pid := findStructPrimaryKeyField(obj)
		if !pid.pkExist {
			return
		}
		s.bindOnePrimaryKey(pid.interfaceValue)
	}
}

// insertManyStruct 插入多个struct
func (s *InsertScope) insertManyStruct(obj interface{}) {
	var tx = std.WithContext(s.ctx).Model(reflect.New(s.scope.modelKind).Interface())
	if len(s.omitFields) != 0 {
		tx.Omit(s.omitFields...)
	}
	if len(s.selectFields) != 0 {
		tx.Select(s.selectFields)
	}
	if s.chunkSize == 0 {
		s.err = tx.Create(obj).Error
	} else {
		s.err = tx.CreateInBatches(obj, int(s.chunkSize)).Error
	}

	if s.rowsAffected == nil {
		s.rowsAffected = new(int64)
	}
	*s.rowsAffected = tx.RowsAffected

	if s.err != nil {
		return
	}
	// 写回 WithPrimaryKey
	if s.withPK {
		s.bindManyPrimaryKey(obj)
	}
}

// One 插入一个
// doc: 目前只支持 *struct, *map
// 传入 *map 时 会捕获主键id的内容以及字段名 存入 主键 column 中
func (s *InsertScope) One(doc interface{}) *InsertScope {
	if s.err != nil {
		return s
	}
	vo := reflect.ValueOf(doc)
	vt := reflect.TypeOf(doc)

	// 非指针 返回错误
	if vo.Kind() != reflect.Ptr {
		s.err = fmt.Errorf("invalid doc type, not ptr")
		return s
	}
	vo = vo.Elem()

	// 插入一个map
	if vo.Kind() == reflect.Map {
		s.insertOneMap(doc)
		return s
	}

	if vo.Kind() != reflect.Struct {
		s.err = fmt.Errorf("invalid doc type, not ptr struct")
		return s
	}

	if vt != s.scope.modelKind {
		s.err = fmt.Errorf("table %s unsupport type: %s", s.scope.tableName, vt.String())
		return s
	}

	// 插入一个struct
	s.insertOneStruct(doc)

	return s
}

// Many 插入多个
// docs: 支持 *[]struct | *[]map[string]interface{}
// 传入 *[]map[string]interface{} 时 只能通过 WithPrimaryKey 获取插入成功的主键
func (s *InsertScope) Many(docs interface{}) *InsertScope {
	if s.err != nil {
		return s
	}
	vt := reflect.TypeOf(docs)
	vo := reflect.ValueOf(docs)

	if vt.Kind() != reflect.Ptr {
		s.err = fmt.Errorf("invalid docs type, not ptr")
		return s
	}

	// 取指针内容
	vt = vt.Elem()
	vo = vo.Elem()

	// 判断是否是切片 数组
	if vt.Kind() != reflect.Array && vt.Kind() != reflect.Slice {
		s.err = fmt.Errorf("invalid docs type, not ptr slice")
		return s
	}
	vt = vt.Elem()

	switch vt.Kind() {
	case reflect.Map:
		s.insertManyMap(docs)
	case reflect.Struct:
		s.insertManyStruct(docs)
	default:
		s.err = fmt.Errorf("unsupport type: %+v", vt.Kind())
		return s
	}

	return s
}

// GetRowsAffected 返回插入影响行数
func (s *InsertScope) GetRowsAffected() (int64, error) {
	return *s.rowsAffected, s.err
}

// Error 返回错误信息
func (s *InsertScope) Error() error {
	return s.err
}
