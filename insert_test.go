package gdbc

import (
	"context"
	"fmt"
	"github.com/sirupsen/logrus"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"reflect"
	"regexp"
	"testing"
	"time"
)

func TestInsertScope_One(t *testing.T) {
	var c = Config{
		DSN: "root:root@tcp(10.0.0.135:3306)/mydb?charset=utf8&parseTime=True&loc=Local",
		Config: &gorm.Config{
			Logger: logger.Default.LogMode(logger.Info),
		},
	}

	if err := InitClient(&c); err != nil {
		panic(err)
	}

	// 插入一个struct
	var m = new(ModelSchedTask)

	//_ = std.AutoMigrate(m) // 迁移表

	scope := NewModel(m)

	var tm = time.Now().Unix()
	var ctx = context.Background()

	var record = &ModelSchedTask{
		CreatedAt: tm,
		UpdatedAt: tm,
	}

	if err := scope.Insert().SetContext(ctx).One(record).Error(); err != nil {
		logrus.Errorf("get err: %+v", err)
		return
	}

	fmt.Println(record.Id)
}

func TestInsertScope_One2(t *testing.T) {
	var c = Config{
		DSN: "root:root@tcp(10.0.0.135:3306)/mydb?charset=utf8&parseTime=True&loc=Local",
		Config: &gorm.Config{
			Logger: logger.Default.LogMode(logger.Info),
		},
	}

	if err := InitClient(&c); err != nil {
		panic(err)
	}

	// 插入一个struct
	var m = new(ModelSchedTask)

	scope := NewModel(m)

	var tm = time.Now().Unix()
	var ctx = context.Background()

	var record = map[string]interface{}{
		"created_at": tm,
		"updated_at": tm,
	}

	//if err := std.Model(m).Create(record).Error; err != nil {
	//	logrus.Errorf("get err: %+v", err)
	//	return
	//}
	var id int64
	if err := scope.Insert().SetContext(ctx).WithPrimaryKey(&id).One(&record).Error(); err != nil {
		logrus.Errorf("get err: %+v", err)
		return
	}

	fmt.Println(record)
	fmt.Println(id)
}

func TestCanSet(t *testing.T) {
	var a = make(map[string]bool)

	var b interface{} = a
	to := reflect.TypeOf(b)
	vo := reflect.ValueOf(b)
	if vo.Kind() == reflect.Ptr {
		vo = vo.Elem()
	}
	fmt.Println(to, vo.CanAddr(), vo.CanSet())
}

func TestGetTag(t *testing.T) {
	var a = "primaryKey;autoIncrement;column:id;type:int;"

	reg := regexp.MustCompile(`column:([\w\-_]*);?`)
	res := reg.FindAllStringSubmatch(a, -1)
	fmt.Println(res)
}

func TestInsertBatch1(t *testing.T) {
	var c = Config{
		DSN: "root:root@tcp(10.0.0.135:3306)/mydb?charset=utf8&parseTime=True&loc=Local",
		Config: &gorm.Config{
			Logger: logger.Default.LogMode(logger.Info),
		},
	}

	if err := InitClient(&c); err != nil {
		panic(err)
	}

	// 插入一个struct
	var m = new(ModelSchedTask)

	scope := NewModel(m)

	var tm = time.Now().Unix()
	var ctx = context.Background()

	var record = &[]ModelSchedTask{
		{
			CreatedAt: tm,
			UpdatedAt: tm,
		},
		{
			CreatedAt: tm + 1,
			UpdatedAt: tm + 1,
		},
		{
			CreatedAt: tm + 2,
			UpdatedAt: tm + 3,
		},
	}

	var keys []int64
	if err := scope.Insert().SetContext(ctx).WithPrimaryKey(&keys).Many(record).Error(); err != nil {
		logrus.Errorf("get err: %+v", err)
		return
	}

	fmt.Println(record)
	fmt.Println(keys)
}

func TestInsertBatch2(t *testing.T) {
	var c = Config{
		DSN: "root:root@tcp(10.0.0.135:3306)/mydb?charset=utf8&parseTime=True&loc=Local",
		Config: &gorm.Config{
			Logger: logger.Default.LogMode(logger.Info),
		},
	}

	if err := InitClient(&c); err != nil {
		panic(err)
	}

	// 插入一个struct
	var m = new(ModelSchedTask)

	scope := NewModel(m)

	var tm = time.Now().Unix()
	var ctx = context.Background()

	var record = []map[string]interface{}{
		{
			"created_at": tm,
			"updated_at": tm,
		},
		{
			"created_at": tm + 10,
			"updated_at": tm + 10,
		},
		{
			"created_at": tm + 20,
			"updated_at": tm + 20,
		},
	}

	var keys []int64
	if err := scope.Insert().SetContext(ctx).WithPrimaryKey(&keys).Many(&record).Error(); err != nil {
		logrus.Errorf("get err: %+v", err)
		return
	}

	fmt.Println(record)
	fmt.Println(keys)
}
