module gitea.com/iota/gdbc

go 1.16

require (
	gitea.com/iota/iota v0.0.0-20220102112836-c0cd94d84906
	github.com/antonfisher/nested-logrus-formatter v1.3.1
	github.com/sirupsen/logrus v1.8.1
	google.golang.org/protobuf v1.27.1
	gorm.io/driver/mysql v1.2.3
	gorm.io/gorm v1.22.4
)
