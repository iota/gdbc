package gdbc

import "gorm.io/gorm"

type Config struct {
	DSN    string       `json:"dsn" yaml:"dsn"` // 连接DSN
	Config *gorm.Config // gorm的配置
}
