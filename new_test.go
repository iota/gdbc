package gdbc

import (
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
	"testing"
	"time"
)

type Product struct {
	gorm.Model
	Code  string
	Price uint
}

func TestNewGorm(t *testing.T) {
	var c = Config{
		DSN: "root:root@tcp(10.0.0.135:3306)/mydb?charset=utf8&parseTime=True&loc=Local",
		Config: &gorm.Config{
			Logger: logger.Default.LogMode(logger.Info),
		},
	}

	if err := InitClient(&c); err != nil {
		panic(err)
	}

	//std.AutoMigrate(&ModelSchedTask{})

	//scope := NewModel(&ModelSchedTask{})
	//
	//var ctx = context.Background()

	// todo 竞态数据测试

	var a = func() {
		for i := 0; i < 10; i++ {
			scope := std.Model(&ModelSchedTask{}).Select("req_id")
			scope.Where("req_id = 1")

			var c int64
			scope.Count(&c)

			var record ModelSchedTask

			scope.Find(&record)
		}
	}

	var b = func() {
		scope := std.Model(&ModelSchedTask{}).Select("id")

		for i := 0; i < 2; i++ {
			scope.Where("id = 1")

			var c int64
			scope.Count(&c)

			var record ModelSchedTask

			scope.Find(&record)
		}
	}

	go a()
	go b()

	time.Sleep(time.Second * 3)
}
