package gdbc

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"google.golang.org/protobuf/proto"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"os"
	"reflect"
	"runtime"
	"strings"

	nested "github.com/antonfisher/nested-logrus-formatter"
)

func init() {
	var getServeDir = func(path string) string {
		var run, _ = os.Getwd()
		return strings.Replace(path, run, ".", -1)
	}
	var formatter = &nested.Formatter{
		NoColors:        false,
		HideKeys:        true,
		TimestampFormat: "2006-01-02 15:04:05",
		CallerFirst:     true,
		CustomCallerFormatter: func(f *runtime.Frame) string {
			s := strings.Split(f.Function, ".")
			funcName := s[len(s)-1]
			return fmt.Sprintf(" [%s:%d][%s()]", getServeDir(f.File), f.Line, funcName)
		},
	}
	logrus.SetFormatter(formatter)
	logrus.SetReportCaller(true)

	// 注册错误码
	register()
}

var std *gorm.DB

// Model 绑定模型
type Model struct {
	Type      proto.Message // 模型原形
	modelKind reflect.Type  // 模型类型
	modelName string        // 模型名称
	tableName string        // 绑定表名
}

// tabler 模型必须实现该接口
type tabler interface {
	TableName() string
}

// GetTableName 获取表名
func GetTableName(i interface{}) string {
	if p, ok := i.(tabler); ok {
		return p.TableName()
	}
	return ""
}

// NewModel 实例化model 返回该 proto 对应的 scope
func NewModel(msg interface{}) *Scope {
	if std == nil {
		panic("database nil")
	}

	if msg == nil {
		panic("proto message nil")
	}

	remsg, ok := msg.(proto.Message)
	if !ok {
		panic("msg no match proto message")
	}
	m := &Model{Type: remsg}

	m.modelKind = reflect.TypeOf(msg)
	m.modelName = string(m.Type.ProtoReflect().Descriptor().FullName())
	m.tableName = GetTableName(m.Type)

	if m.tableName == "" {
		panic(fmt.Errorf("%s not impl tabler interface or TableName() return empty string", m.modelName))
	}

	s := &Scope{
		Model: m,
	}
	return s
}

// InitClient 初始化mysql的连接客户端
func InitClient(c *Config) (err error) {
	std, err = gorm.Open(mysql.Open(c.DSN), c.Config)
	if err != nil {
		logrus.Errorf("get err: %+v", err)
		return err
	}
	return nil
}
