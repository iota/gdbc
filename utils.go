package gdbc

import (
	"reflect"
	"regexp"
	"strings"
)

// pkField 主键字段信息
type pkField struct {
	valueOf        reflect.Value       // 字段值的value
	interfaceValue interface{}         // 字段值value的interface
	fieldName      string              // 字段名
	columnName     string              // 表字段名
	fieldOf        reflect.StructField // 字段类型
	typeOf         reflect.Type        // 字段类型
	pkExist        bool                // 主键字段是否存在
}

// findStructPrimaryKeyField 找主键字段
func findStructPrimaryKeyField(obj interface{}) pkField {
	var pkField = pkField{}
	vo := reflect.ValueOf(obj)
	vt := reflect.TypeOf(obj)
	if vt.Kind() == reflect.Ptr {
		vt = vt.Elem()
	}
	if vo.Kind() == reflect.Ptr {
		vo = vo.Elem()
	}

	for i := 0; i < vt.NumField(); i++ {
		field := vt.Field(i)
		if field.PkgPath != "" {
			continue
		}
		tag := strings.ToLower(field.Tag.Get("gorm"))
		if strings.Contains(tag, "primarykey") {
			pkField.pkExist = true
			pkField.fieldOf = field
			pkField.typeOf = field.Type
			break
		}
	}

	if !pkField.pkExist {
		return pkField
	}

	// 表字段名获取
	tag := strings.ToLower(pkField.fieldOf.Tag.Get("gorm"))
	if strings.Contains(tag, "primarykey") {
		reg := regexp.MustCompile(`column:([\w\-_]*);?`)
		res := reg.FindAllStringSubmatch(pkField.fieldOf.Tag.Get("gorm"), -1)
		if len(res) == 1 && len(res[0]) == 2 {
			pkField.columnName = res[0][1]
		}
	}

	// 结构体字段名获取
	pkField.fieldName = pkField.fieldOf.Name
	// 字段内容值获取
	pkField.valueOf = vo.FieldByName(pkField.fieldOf.Name)
	// 字段内容值interface获取
	pkField.interfaceValue = vo.FieldByName(pkField.fieldOf.Name).Interface()

	return pkField
}

// newModelValue 新建一个基于反射构建的 表结构
func newModelValue(k reflect.Type) reflect.Value {
	return reflect.New(k)
}
